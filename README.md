# README #

Presented with a character matrix and a large stream of words, your task is to create a Class
that searches the matrix to look for the words from the word stream. Words may appear
horizontally, from left to right, or vertically, from top to bottom. 

### Rules ###

* The WordFinder constructor receives a set of strings which represents a character matrix. The
matrix size needs to be 64x64, all strings contain the same number of characters.
The "Find" method should return the top 10 most repeated words from the word stream found in
the matrix. If no words are found, the "Find" method should return an empty set of strings. If any
word in the word stream is found more than once within the matrix, the search results should
count it only once

### Notes ###
I tried to use recursion but my solution failed when there where repeated consecutively characters in the matrix and match with the initial form any word in the wordstream

        i.e.      if we were searching for friend
                    and in the matrix were the line :                
                        a s f f r i e n d r t h 

                    the f before friend made that word couldn't be found
                    and when I tried to return to that index in code, I got a stack oveerflow exception.(too many recursive calls)

So I decided to go with the other approach
