﻿using System;
using System.Collections.Generic;

namespace WordFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            var matrix = new List<string>() {
                "zgzsqucrhttsjwibagctbnozazjgheennrehfdftlmuafvjecbhwezijtyglscfb",
"rcjpdmnnommwquradlagubvxolyzhsoipoavzarvuyamcrfhymtcqkircqrrchrg",
"eqjrcsfpglitchyrxrlogzaybblzbtylibitiaakfitsfoaapfvltfcikwaasaoz",
"faylqvrseuzaxjdrdbiomyfmnflyeesbknmtirmdwtgittteopdeohonwrvewiwp",
"lxzlreyiccrepolevedvwqjkztbbpwesqhonaudrajgucepureqlrhsnetipgnnl",
"exivwdbfaowgobfluithealzyioluknknifrirgtllxanvrlgomzrlupirtjlmjr",
"cryotrfjltdwuoroadnlegnnwoxnnrqfitrdediynbpstsylanazazouvwyjbaaw",
"talznhtmpfwnsymgdflezzmckpimmpgtfyzabpnpuxlouedfxueuclresdipuiel",
"ifajlwrtewxtojcgxcxfjfkneaczoftswrumbtpvseiesswmwlupefxthnqaglhs",
"omcwnenerwebapiidqnyvgqxthjpjvivwsyazrtezeurvkdatawvelrtfsbcukar",
"nktoxmlognfybwermbmddefpztuiyjgcaangjritpcretzqtyuelcimtluuyvdhb",
"fjapnmlbieikedziafjmnuaqxlyzcwhnwwxeckeqtnmqdaofydrxnnmczksrihap",
"zarxifemaltwrhfdsnokjcviajbgthxdjrmowrlhctilgbmvwbsgkdklhuzaizsm",
"atztvlirhtenuisuoleaspatngienbkiipdlpbbaktngufhrshsrcinszcinlith",
"xnoxtqndjgodigytkovnayihljvaivezaxhibfbhwcojhjoznonvfeyzmbkrotzo",
"pnstvdeiepffnseershatopvcrpxvfgjrgeqltesbpsgimokwaoetcjdrriylbiv",
"mdoxjwadinwddalnoihindzrjpauopklexcsxnlxjfhowpszbcixdhrqabusblyq",
"ibmtahoumccprldtejnjnyosrhltgmsskfngzxihxvnaxsudykwpvagssnxkewes",
"keosxclltjmeigoryyvgxdwubaqllihckhavwrkwuxuonpkaenwaaedveodsfcnq",
"shojelixlayronyafjfdfhclectqbvqqdvwfihtbspidermanfauxcuaalreuonr",
"jjdbujattzyuptvnnurmrvqcxwxiqrwkgxopjxilpzyjyayohvemossyrdhilwth",
"xtdhhpmsppmzmrgcqjumoimrfqqouyhkaylkabkjdrcsdwtsjdkaqctgmsotbifn",
"mwbxqjnqzflamaielkitbmswoetolgaarzlhpgwwhrzqetwamdudtxvfsmzcalof",
"nlkrfpimdxtokmtskttmlyfednmflwcwyeadngxfexjyuddvhkuuwjtkytceobkn",
"gptryjacwrsamsvpzjetgmjuqfvuwegxwebapicaexomuxclianoccneofrwaile",
"lscdqshiisyzpgrqowrjddgpvynogardfrcxcbtwzwurxolgbnruyonlxqewrwye",
"gsketkcxyqwcqqctrnrsbunailtyejmqintegerltrwwnbarrelpcizhdrekykqh",
"riklsdbhnylarjqdkcubzdnabkcortbzvmoinbfvdjxsuleuzzdovoazpbfwjlna",
"itbzwbrktpjnhvsrinpkclzaoqdlpsqjcykkbxyqxzomizlxghsewjfofqiwjblp",
"dowzkymlxyemptjgrmibhramxpeoitupruobhgienljebradxchtxbthtkagoubd",
"psvucnaetamuvfwgaijctthenvkbboezgyyoaveoecptjymfjpproitibxwmkgxh",
"zfnpispidermanaczwxatfaqihlkblmktramsjqvwtoiexyhjlfyaoplgikbwpoz",
"nivliqogdbedditwfzmvzxhfhaweztlibnsrwceoydzizmjhzagkrpkqaavmkhcz",
"xxqyqkhyfxapeglitchvjijtonailoebkzecytymeqmxcmaohwlneseevvypllzg",
"betnfdnlhhphyvnedubqzzohlrvkvbexnnmmiynpgnitirwmpwhyneyppndtbodp",
"ythpnuoioqsfjbeeefcsafljzawvlhcatkdqrmaegofgegxzmicmnjkiwzogfyvh",
"nrstfibiqtkzfrrliyoeytlxwbearvnjvykxkrmhmprotectionbapfdyqtlanxv",
"eyeluispidermannoconfidencewrsazshcrtacxynjocwqhtrcimiomcabzregu",
"kcoftrkyjddredaoxpimeqnkqcpxsdrhlskufwlhsfrbukuwbrtrotrmcyqhsbiw",
"dcnilttsdwsponshpxeakjlicmfegwtgagrqwqbpxcyedwmibkkiwntpbuiccewk",
"kcoereytnwiycdpjhxrraipwsennehnfregngckylkwsvzkncbupoxurbehfrsqe",
"pniurzcppoleiequovmicsveeeicdseaqgdhilhheuahdofwmmrlgwnpqzzmicil",
"cenozectbzrolsbvseiasifurrjoircrnfussoltmcpczpgkbgrrzgevllwmluzu",
"phndejfaiqugkvoftfjznebatordtmsquthegycrukvgmdkoxeheutygctqmtwmd",
"omywuomejoihcatsipfvnnwswtceoyccqkwssilwihfpczmpkmpmfaeakbnyjnre",
"pdyvmvrjrxnbzzvcnmprqaewqhwdrjxannpyltfztzxawibrqbaidxmodgowxgbh",
"ucprbdcpceafocietfprxzrrngitfsvynbqfmwmdcyvsyiohrqrjnegnpviisoqc",
"lxvyeoksguotepvhepvdwmkxjbttffjzjhnpezebuiysxwgjnbethjqlhmsfavns",
"ajmopvjqewnsfpfwglidwhxjmtabyzsffoealxyotjrenoitullopgztlksmxolh",
"tyzflkiieashvpeyewjgzkidydrpkgrkcfmwpphrbdmnfixprleazfytqvuviean",
"ipwvkeplrrfcfzzdrlosnrrwvljpqndanzdglxkepmzgsalarywkuimvotcsbsyl",
"ovptbfeueamooszohpiueptpvamciiqlbopbwkwnkzbemkmegvlcpsnxgtsvfvei",
"nptcqyachdprintergobanarnaoejdppbpikwgbkbahrolaodxtyiqfueeiiszrx",
"cirnktwwvccmiftiqnprotmxkmlvxrnudgotsnakvjtdnllaxihceivyrhdcoztg",
"khtrsbscnvsqnefyekakdopxibbyloizprrzclfypyfsighnwyikxyeptmolsrdq",
"mseeuhjjlyscsmnahcwtgwiwvkywzcolvpumweswkuetiegdmwblmderphfxkium",
"krrgroikbrirpucshwkcmqddzrnphexxeypqjslnfuysemytdnbakdsyrysegohm",
"feqsbfhzvqigebzukoobetonafavnrgbhvsawvrefgcyttxmrwrxkxftzzqvccon",
"lbpowafclbaychtplxudblmmlenevveqogkemgosbcjoceivvmccnatrloogwgvo",
"nmwyzqhtiobategxtwdhxvmenhqbdqgsufbsfiloahtjpxrhwbucktbojtmfzvwm",
"dekgesajzuhjoueycwmptujxvokpsosxcplkpuyrrdnsryadqxnskzjdleyzkfno",
"gmriilbqghrqriiajudlsxeifcjofbtccdgkvmqyyxgvoskaalwlmpgdzkaifskr",
"wmpjzqwuidelttobczzksgkbhygvksxibrotherbqnafwtoaiycsmtwsqtrdedqx",
"ohapkffjcfibcertaehwqdjnvhtdxoqqemuehrwdyeefccviuvdtovntmnewzqmj"
                };
            var wordFinder = new WordFinder(matrix);
            var wordstream = new List<string>() { "drive","glitch","code","string","bug","glitch","nail","integer","developer","confidence","entrance","reflection","notebook","population","fruit","barrel","mossy","dandelion","chainmail","flowerpot","matrix",
"replace","chill","drive","notebook","spiderman","rise","dragon","captain","table","rockband","guitar","smart","puzzle","bottle","allowance","integer","developer","webapi","nail","master","city","damage",
"string","bug","spiderman","glitch","drive","matrix","nail","integer","developer","pearl","item","respiration","vine","vex","carrot","mutton","matrix","gateway","fruit","integer","barrel","mossy","dandelion",
"drive","chainmail","flowerpot","defeat","table","frown","gravity","matrix","poetry","train","unlawful","console","integer","create","spiderman","pure","neighbour","analyst","drive","pepperoni","salt","pretzels",
"pepper","relish","mayonnaise","oatmeal","matrix","abalone","doughnut","venison","integer","sausage","cake","pear","pistachio","summary","printer","bottle","flock","forward","wealth","wheat","membership",
"reference","matrix","recording","notebook","awareness","pistachio","truth","restaurant","delivery","confidence","spiderman","entrance","reflection","string","bug","glitch","nail","integer","developer",
"population","manner","inspector","awareness","finance","rise","parachute","editor","exhibition","code","nail","government","rent","yesterday","brother","opinion","guest","passenger","fortune","loss",
"manner","poet","election","discussion","reality","schedule","writing","mistake","departure","layer","worker","conflict","salary","depression","protection","pollution","string","notebook","bug","glitch",
"branch","matrix","awareness","chill","inspector","spiderman","fruit","barrel","mossy","glitch","dandelion","chainmail","flowerpot","glitch","notebook","spiderman","dragon"};
            var result = wordFinder.Find(wordstream);
            foreach(var res in result)
            {
                Console.WriteLine(res.Word + " - " + res.TimesRepeated);
            }
            Console.ReadLine();

        }
    }

}
