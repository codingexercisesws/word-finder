﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace WordFinder
{
    public class WordFinder
    {
        private List<string> _matrix;
        public WordFinder(List<string> matrix)
        {
            _matrix = matrix;
        }

        public IEnumerable<RepeatedWord> Finds(IEnumerable<string> wordstream)
        {
            var repeatedWords = new List<RepeatedWord>();
            var verticalMatrix = GetVerticalMatrix().ToList();
            
            for (int i = 0; i < _matrix.Count(); i++)
            {
                var wordHorizontal = _matrix[i];
                var wordVertical = verticalMatrix[i];

                foreach (var word in wordstream)
                {
                    if (wordHorizontal.Contains(word) || wordVertical.Contains(word))
                    {
                        if(!repeatedWords.Any(x => x.Equals(word)))
                            repeatedWords.Add(
                                new RepeatedWord {
                                    Word = word,
                                    TimesRepeated = 1
                            });
                        repeatedWords.Where(w => w.Word == word).FirstOrDefault().TimesRepeated++;
                    }
                }
            }
            return repeatedWords;
        }

        public IEnumerable<RepeatedWord> Find(IEnumerable<string> wordstream)
        {
            var repeatedWords = new List<RepeatedWord>();
            var verticalMatrix = GetVerticalMatrix().ToList();
            foreach (var word in wordstream)
            {
                if (word == "code")
                    Console.WriteLine("hey");

                for (int i = 0; i < _matrix.Count(); i++)
                {
                    var wordHorizontal = _matrix[i];
                    var wordVertical = verticalMatrix[i];

                    if (wordHorizontal.Contains(word) || wordVertical.Contains(word))
                    {
                        if (!repeatedWords.Any(x => x.Word.Equals(word)))
                        {
                            repeatedWords.Add(
                                  new RepeatedWord
                                  {
                                      Word = word,
                                      TimesRepeated = 1
                                  });
                        }
                        else
                        {
                            repeatedWords.Where(w => w.Word == word).FirstOrDefault().TimesRepeated++;
                        }
                        break;
                    }
                }
                    
            }
            return repeatedWords.OrderByDescending(w => w.TimesRepeated).Take(10);
        }

        private IEnumerable<string> GetVerticalMatrix()
        {
            var verticalMatrix = new string[_matrix.Count];

            foreach (var item in _matrix)
            {
                for (int i = 0; i < item.Length; i++)
                {
                    var chars = item.ToCharArray();

                    verticalMatrix[i] += chars[i].ToString();
                }
            }

            return verticalMatrix;
        }


        /*I tried to use recursion but my solution failed when there where repeated consecutively characters in the matrix and match with the initial form any word in the wordstream

                 i.e.      if we were searching for friend
                             and in the matrix were the line :                
                                 a s f f r i e n d r t h 

                             the f before friend made that word couldn't be found
                             and when I tried to return to that index in code, I got a stack oveerflow exception.(too many recursive calls)
           
        So I decided to go with the other approach
          */
        /*
               private List<string> _matrix;
         public WordFinder(List<string> matrix)
         {
             _matrix = matrix;
         }
         private char[][] GenerateCharMatrix()
         {
             int length = _matrix.Count;
             char[][] charMatrix = new char[length][];

             for (int i = 0; i < length; i++)
             {
                 charMatrix[i] = _matrix[i].ToCharArray();
             }
             return charMatrix;
         }

         private bool searchHorizontally(string searchingWord, char[][] charMatrix, int indexh, int indexv, int indexSW, int cont, int missingCont)
         {
             if (cont == searchingWord.Length) return true;

             if (indexh >= _matrix.Count || indexSW >= _matrix.Count || indexv >= _matrix.Count) return false;

             if (indexh + missingCont > _matrix.Count) return searchHorizontally(searchingWord, charMatrix, 0, indexv + 1, 0, 0, searchingWord.Length);

             var letraBuscada = searchingWord.ElementAt(indexSW);
             var letraComparar = charMatrix[indexv][indexh];

             if (searchingWord.ElementAt(indexSW) == charMatrix[indexv][indexh])
                 return searchHorizontally(searchingWord, charMatrix, indexh + 1, indexv, indexSW + 1, cont + 1, missingCont - 1);

             return searchHorizontally(searchingWord, charMatrix, indexh + 1, indexv, 0, 0, searchingWord.Length);
         }

         private bool searchVertically(string searchingWord, char[][] charMatrix, int indexh, int indexv, int indexSW, int cont, int missingCont)
         {
             if (cont == searchingWord.Length) return true;

             if (indexh >= _matrix.Count || indexSW >= _matrix.Count || indexv >= _matrix.Count)
                 return false;

             if (indexv + missingCont > _matrix.Count)
                 return searchVertically(searchingWord, charMatrix, indexh + 1, 0, 0, 0, searchingWord.Length);

             var letraBuscada = searchingWord.ElementAt(indexSW);
             var letraComparar = charMatrix[indexv][indexh];
             if (searchingWord.ElementAt(indexSW) == charMatrix[indexv][indexh])
                 return searchVertically(searchingWord, charMatrix, indexh, indexv + 1, indexSW + 1, cont + 1, missingCont - 1);
             return searchVertically(searchingWord, charMatrix, indexh, indexv + 1, 0, 0, searchingWord.Length);
         }

         public IEnumerable<string> Find(IEnumerable<string> wordstream)
         {
             var charMatrix = GenerateCharMatrix();


             var foundedWords = new List<string>();

             for (int i = 0; i < wordstream.Count(); i++)
             {
                 var searchingWord = wordstream.ElementAt(i);
                 var indexSW = 0;
                 if ((searchHorizontally(searchingWord, charMatrix, 0, 0, indexSW, 0, searchingWord.Length) && searchVertically(searchingWord, charMatrix, 0, 0, indexSW, 0, searchingWord.Length)) ||
                       && !foundedWords.Any(x => x.Equals(searchingWord))
                     )
                 {
                     foundedWords.Add(searchingWord);
                 }

             }
             return foundedWords;
         }


          */
    }
}
